<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>ExamenBootStradStudio</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/styles.css">
</head>

<body>
    <nav class="navbar navbar-light navbar-expand-md text-center" style="background-color: #1F1F1F;">
        <div class="container-fluid"><button data-toggle="collapse" class="navbar-toggler" data-target="#navcol-1"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button><img src="assets/img/logo.png">
            <div class="collapse navbar-collapse text-center"
                id="navcol-1">
                <ul class="nav navbar-nav">
                    <li class="nav-item" role="presentation"><a class="nav-link active" href="#" style="color: rgba(255,255,255,0.9);">Home&nbsp;</a></li>
                </ul>
                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#" style="color: rgb(255,255,255);">Pages&nbsp;</a>
                    <div class="dropdown-menu" role="menu"><a class="dropdown-item" role="presentation" href="#">About us</a><a class="dropdown-item" role="presentation" href="#">Menu</a><a class="dropdown-item" role="presentation" href="#">News and Events</a><a class="dropdown-item" role="presentation"
                            href="#">Chefs</a><a class="dropdown-item" role="presentation" href="#">Coming Soon</a><a class="dropdown-item" role="presentation" href="#">404 page</a></div>
                </li>
                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#" style="color: rgb(255,255,255);">Portfolio&nbsp;</a>
                    <div class="dropdown-menu" role="menu"><a class="dropdown-item" role="presentation" href="#">Portfolio 2 columns</a><a class="dropdown-item" role="presentation" href="#">Portfolio 3 columns<br></a><a class="dropdown-item" role="presentation" href="#">Portfolio 4 columns<br></a></div>
                </li>
                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#" style="color: rgb(255,255,255);">Blog</a>
                    <div class="dropdown-menu" role="menu"><a class="dropdown-item" role="presentation" href="#">Blog with right slider</a><a class="dropdown-item" role="presentation" href="#">Blog with left slider</a><a class="dropdown-item" role="presentation" href="#">Article category blog</a></div>
                </li>
                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#" style="color: rgb(255,255,255);">Post formats&nbsp;</a>
                    <div class="dropdown-menu" role="menu"><a class="dropdown-item" role="presentation" href="#">Standard post format</a><a class="dropdown-item" role="presentation" href="#">Video post format</a><a class="dropdown-item" role="presentation" href="#">Audio post format</a>
                        <a
                            class="dropdown-item" role="presentation" href="#">Galery post format</a><a class="dropdown-item" role="presentation" href="#">Link post format</a><a class="dropdown-item" role="presentation" href="#">Quote link post format</a><a class="dropdown-item" role="presentation" href="#">Status post format</a>
                            <a
                                class="dropdown-item" role="presentation" href="#">Image post format</a>
                    </div>
                </li>
                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#" style="color: rgb(255,255,255);">Joomla</a>
                    <div class="dropdown-menu" role="menu"><a class="dropdown-item" role="presentation" href="#">Conntent component</a><a class="dropdown-item" role="presentation" href="#">Contact component</a><a class="dropdown-item" role="presentation" href="#">Users component</a><a class="dropdown-item"
                            role="presentation" href="#">Other components</a></div>
                </li>
                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" href="#" style="color: rgb(255,255,255);">K2 Blog</a>
                    <div class="dropdown-menu" role="menu"><a class="dropdown-item" role="presentation" href="#">Blog listing</a><a class="dropdown-item" role="presentation" href="#">Single blog</a><a class="dropdown-item" role="presentation" href="#">User's blog</a></div>
                </li>
            </div>
        </div>
    </nav>
    <div class="carousel slide" data-ride="carousel" id="carousel-1">
        <div class="carousel-inner" role="listbox">
            <div class="carousel-item"><img class="w-100 d-block" src="assets/img/carousel.jpg" alt="Slide Image"></div>
            <div class="carousel-item"><img class="w-100 d-block" src="assets/img/carousel2.jpg" alt="Slide Image"></div>
            <div class="carousel-item active"><img class="w-100 d-block" src="assets/img/carousel3.jpg" alt="Slide Image"></div>
        </div>
        <div><a class="carousel-control-prev" href="#carousel-1" role="button" data-slide="prev"><span class="carousel-control-prev-icon"></span><span class="sr-only">Previous</span></a><a class="carousel-control-next" href="#carousel-1" role="button" data-slide="next"><span class="carousel-control-next-icon"></span><span class="sr-only">Next</span></a></div>
        <ol
            class="carousel-indicators">
            <li data-target="#carousel-1" data-slide-to="0"></li>
            <li data-target="#carousel-1" data-slide-to="1"></li>
            <li data-target="#carousel-1" data-slide-to="2" class="active"></li>
            </ol>
    </div>
    <div>
        <div class="container">
            <p class="text-center" style="font-size: 30px;padding: 40px;color: rgb(218,16,16);"><strong>WELCOME TO AT RESTAURANT</strong></p>
            <p class="text-center"><br>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et<br>ut enim ad minim veniam, quis nostrud exercitation ullamco.<br><br></p><i class="fa fa-spoon border rounded-0" style="font-size: 93px;width: 64px;margin: 39px;padding: 10px;"></i>
            <i
                class="fa fa-coffee" style="font-size: 90px;height: 83px;margin: 42px;padding-left: 48px;"></i><i class="fa fa-beer" style="font-size: 90px;margin: 10px;padding-left: 27px;"></i><i class="fa fa-trophy" style="font-size: 90px;padding: 0px;padding-left: 105px;"></i>
                <div class="row text-center justify-content-start align-items-start">
                    <div class="col-2 text-center">
                        <h6><strong>BEST CUISINE</strong><br></h6>
                        <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit.ullamcorper diam nec augue semper, in dignissim.<br></p>
                    </div>
                    <div class="col-2 text-center">
                        <h6 class="text-center"><strong>SPECIAL OFFERS</strong><br></h6>
                        <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit.ullamcorper diam nec augue semper, in dignissim.<br></p>
                    </div>
                    <div class="col-2 text-center">
                        <h6 class="text-center"><strong>GOOD REST</strong><br></h6>
                        <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit.ullamcorper diam nec augue semper, in dignissim.<br></p>
                    </div>
                    <div class="col-2 text-center">
                        <h6 class="text-center"><strong>BEST CUISINE</strong><br></h6>
                        <p class="text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit.ullamcorper diam nec augue semper, in dignissim.<br></p>
                    </div>
                </div>
        </div>
        <div class="container">
            <p class="text-center" style="font-size: 30px;padding-top: 31px;padding-bottom: 30px;"><strong>LATEST WORK</strong><br></p>
            <p class="text-truncate text-center">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et<br>ut enim ad minim veniam, quis nostrud exercitation ullamco.<br></p>
            <div><img src="assets/img/Latest%20work.PNG"></div>
        </div>
    </div>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>

    <?php
    $servername = "localhost";
    $username = "root";
    $password = "";
    $dbname = "database";
    
    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    
    $sql = "SELECT  * FROM site ";
    $result = $conn->query($sql);
    
    ?>

<?php
        if ($result->num_rows > 0) {
        // output data of each row
        while($row = $result->fetch_assoc()) {
?>
<section id="tabs" class="project-tab">
            <div class="container">
                <div class="row">
                                <table class="table" >
                                    <thead>
                                        <tr>
                                            <th>Nume</th>
                                            <th>Categorie</th>
                                            <th>imagine</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                        <td><?php  echo  $row["nume"]    ?></td>
                                        <td><?php  echo  $row["categorie"]    ?></td>
                                        <td><?php  echo  $row["imagine"]    ?></td>
                                        </tr>
                                    </tbody>
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
                      
                 
                      
                    </tr>
                    
 <?php
        }
        
   
    } else {
    echo "0 results";
}
$conn->close();
       


 ?>  


</body>

</html>